LIBRARY ieee; 
USE ieee.std_logic_1164.all ; 
USE ieee.std_logic_unsigned.all; 

ENTITY part2_FSM_CASE IS
	PORT (LEDR : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
			LEDG : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
			KEY : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
			SW : IN STD_LOGIC_VECTOR(1 DOWNTO 0));
END part2_FSM_CASE;

	ARCHITECTURE Behavior of part2_FSM_CASE  IS

SIGNAL clk : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
SIGNAL w : STD_LOGIC;
SIGNAL z : STD_LOGIC;
CONSTANT A: STD_LOGIC_VECTOR(3 DOWNTO 0) := "0000";
CONSTANT B: STD_LOGIC_VECTOR(3 DOWNTO 0):= "0001";
CONSTANT C: STD_LOGIC_VECTOR(3 DOWNTO 0):= "0010";
CONSTANT D: STD_LOGIC_VECTOR(3 DOWNTO 0):= "0011";
CONSTANT E: STD_LOGIC_VECTOR(3 DOWNTO 0):= "0100";
CONSTANT F: STD_LOGIC_VECTOR(3 DOWNTO 0):= "0101";
CONSTANT G: STD_LOGIC_VECTOR(3 DOWNTO 0):= "0110";
CONSTANT H: STD_LOGIC_VECTOR(3 DOWNTO 0):= "0111";
CONSTANT I: STD_LOGIC_VECTOR(3 DOWNTO 0):= "1000";
SIGNAL y_present, y_next : STD_LOGIC_VECTOR(3 DOWNTO 0);


BEGIN

	clk <= KEY(0);
	reset<=SW(0);
	w<=SW(1);
	
	PROCESS(w,y_present)
	BEGIN
		CASE y_present IS
			WHEN A=>
				z<='0';
				IF(w = '0') THEn
					y_next <= B;
				ELSE
					y_next <= F;
				END IF;
			WHEN B=>
				z<='0';
				IF(w = '0') THEn
					y_next <= C;
				ELSe 
					y_next <= F;
				END IF;
			WHEN C=>
				z<='0';
				IF(w = '0') THEn
					y_next <= D;
				ELSe 
					y_next <= F;
				END IF;
			WHEN D=>
				z<='0';
				IF(w = '0') THEn
					y_next <= E;
				ELSe 
					y_next <= F;
				END IF;
			WHEN E=>
				z<='1';
				IF(w = '0') THEn
					y_next <= E;
				ELSe 
					y_next <= F;
				END IF;
			WHEN F=>
				z<='0';
				IF(w = '1') THEn
					y_next <= G;
				ELSe 
					y_next <= B;
				END IF;
			WHEN G=>
				z<='0';
				IF(w = '1') THEn
					y_next <= H;
				ELSe 
					y_next <= B;
				END IF;
			WHEN H=>
				z<='0';
				IF(w = '1') THEn
					y_next <= I;
				ELSe 
					y_next <= B;
				END IF;
			WHEN I=>
				z<='1';
				IF(w = '1') THEn
					y_next <= I;
				ELSe 
					y_next <= B;
				END IF;
			WHEN OTHERS=>
				z<='0';
				y_next<=A;
		END CASE;
	END PROCESS;
	
	PROCESS(clk)
	BEGIN
	IF (clk'event AND clk = '1') then
		IF(reset = '0') THEn
			y_present<=A;
		ELSE
			y_present <= y_next;
		END IF;
	END IF;
	END PROCESS;
	LEDR(3 DOWNTO 0)<=y_present;
	LEDG(0)<=z;

END Behavior;
			
			