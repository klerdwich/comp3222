LIBRARY ieee; 
USE ieee.std_logic_1164.all ; 
USE ieee.std_logic_unsigned.all; 

ENTITY part3_shiftReg IS
	PORT (LEDR : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
			LEDG : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
			KEY : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
			SW : IN STD_LOGIC_VECTOR(1 DOWNTO 0));
END part3_shiftReg;

ARCHITECTURE Behavior of part3_shiftReg  IS

COMPONENT shiftL IS
	PORT (temp : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			w : IN STD_LOGIC;
			tempOut : OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END COMPONENT shiftL;

SIGNAL clk : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
SIGNAL w : STD_LOGIC;
SIGNAL z : STD_LOGIC;
SIGNAL temp0,temp1 : STD_LOGIC_VECTOR(3 DOWNTO 0);


BEGIN

	clk <= KEY(0);
	reset<=SW(0);
	w<=SW(1);
	
	PROCESS(clk)
	BEGIN
	IF (clk'event AND clk = '1') then
		IF(reset = '0') THEn
			temp0<="1111";
			temp1<="0000";
		ELSE
		
			IF(w='1') then
				 temp0 <= "1111";
				 for i in 0 to 2 loop
					temp1(i) <= temp1(i+1);
				 end loop;
				 temp1(3) <= '1';
			else
				temp1 <= "0000";
				 for i in 0 to 2 loop
					temp0(i) <= temp0(i+1);
				 end loop;
				 temp0(3) <= '0';
			end if;
			
		END IF;
	END IF;
	IF (temp1 = "1111" OR temp0 = "0000") then
				z<='1';
			else
				z<='0';
			end if;
	END PROCESS;
	LEDR(3 DOWNTO 0)<=NOT (temp0(3 DOWNTO 0));
	LEDR(7 DOWNTO 4)<=temp1;
	LEDG(0)<=z;

END Behavior;
			