LIBRARY ieee; 
USE ieee.std_logic_1164.all ; 
USE ieee.std_logic_unsigned.all; 

ENTITY part1_4FSM IS
	PORT (LEDR : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
			LEDG : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
			KEY : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
			SW : IN STD_LOGIC_VECTOR(1 DOWNTO 0));
END part1_4FSM;

ARCHITECTURE Behavior of part1_4FSM IS

COMPONENT D_FF IS
	PORT ( D,clk : IN STD_LOGIC;
	Q : OUT STD_LOGIC);
END COMPONENT D_FF;


SIGNAL clk : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
SIGNAL w : STD_LOGIC;
SIGNAL z : STD_LOGIC;
SIGNAL y: STD_LOGIC_VECTOR(8 DOWNTO 0);

BEGIN

	clk <= KEY(0);
	reset<=SW(0);
	w<=SW(1);

	
	y8: D_FF PORT MAP (NOT reset, clk, Q=> y(8));
	y7:  D_FF PORT MAP(NOT(w) AND(y(8) OR y(3) OR y(2) OR y(1) OR y(0)) AND reset, clk, Q=>y(7));
	y6: D_FF PORT MAP(NOT(w) and y(7) AND reset,  clk, Q=>y(6));
	y5: D_FF PORT MAP(NOT(w) and y(6) AND reset, clk, Q=>y(5));
	y4: D_FF PORT MAP(NOT(w) AND (y(5) OR y(4)) AND reset,  clk, Q=>y(4));
	y3: D_FF PORT MAP(w AND (y(8) or y(7) or y(6) or y(5) OR y(4)) AND reset, clk, Q=>y(3));
	y2: D_FF PORT MAP(w and y(3) AND reset,  clk, Q=>y(2));
	y1: D_FF PORT MAP(w and y(2) AND reset,  clk, Q=>y(1));
	y0: D_FF PORT MAP(w and (y(1) OR y(0)) AND reset,  clk, Q=>y(0));

	LEDR<=y;
	LEDG(0)<=y(4) OR y(0);

END Behavior;


LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY D_FF IS
	PORT ( D,clk : IN STD_LOGIC;
	Q : OUT STD_LOGIC);
END D_FF;
ARCHITECTURE Structural OF D_FF IS
BEGIN
		PROCESS (D,Clk)
		BEGIN
		IF (clk'Event AND clk = '1') THEN
			Q<=D;
		END IF;
	END PROCESS;
END Structural;
			
			