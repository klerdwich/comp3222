LIBRARY ieee; 
USE ieee.std_logic_1164.all ; 
USE ieee.std_logic_unsigned.all; 

ENTITY part4_morse IS --This will be our 'FSM'
	PORT (LEDR : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
			CLOCK_50 : IN STD_LOGIC;
			KEY : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
			SW : IN STD_LOGIC_VECTOR(2 DOWNTO 0));
END part4_morse;

ARCHITECTURE Behavior of part4_morse  IS

COMPONENT letter_sel IS
	PORT(letter : in STD_LOGIC_VECTOR(2 DOWNTO 0);
		  countOut : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		  morseOut : OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END COMPONENT letter_sel;


SIGNAL clk : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
SIGNAL w : STD_LOGIC;
SIGNAL z : STD_LOGIC;
SIGNAL clk_enable : STD_LOGIC;
SIGNAL y_present, y_next : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL count : STD_LOGIC_VECTOR(2 DOWNTO 0);
signal i : integer range 0 to 3;
signal morse: STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL timeCounter:STD_LOGIC_VECTOR(25 DOWNTO 0);

BEGIN

	clk <= KEY(1);
	reset<=KEY(0);
	w<=SW(1);
	
	letterSelect: letter_sel PORT MAP (SW, countOut => count,morseOut => morse );

	process (clk,reset)
	begin
		if(reset = '0') then
			clk_enable <= '0';
		else
			 if	(clk'event AND clk = '1') then
				clk_enable <= '1';
			 end if;
		end if;
	end process;	
	
	process (CLOCK_50) 
   begin
		if(clk_enable ='1' and i <= count) then
			if (CLOCK_50'event and CLOCK_50='1') then
				if (timeCounter >= 25000000 AND morse(i) = '0') then
					timeCounter <= "00000000000000000000000000";
					LEDR(0)  <= '0';
					i <= i + 1;
				elsif (timeCounter >= 50000000 AND morse(i) = '1') then
					timeCounter <= "00000000000000000000000000";
					LEDR(0) <= '0';
					i <= i + 1;
				else
					timeCounter <= timeCounter+1;
					if(timeCounter >= 10000000) then
						LEDR(0)  <= '1';
					end if;
				end if;
			end if;
		else 
			i <= 0;
			timeCounter <= "00000000000000000000000000";
			LEDR(0) <= '0';
		end if;
   end process;

END Behavior;
			
			
			
LIBRARY ieee; 
USE ieee.std_logic_1164.all ; 
USE ieee.std_logic_unsigned.all; 

ENTITY letter_sel IS
	PORT(letter : in STD_LOGIC_VECTOR(2 DOWNTO 0);
		  countOut : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
		  morseOut : OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END letter_sel;

ARCHITECTURE Behavior of letter_sel  IS

CONSTANT A: STD_LOGIC_VECTOR(2 DOWNTO 0) := "000";
CONSTANT B: STD_LOGIC_VECTOR(2 DOWNTO 0):= "001";
CONSTANT C: STD_LOGIC_VECTOR(2 DOWNTO 0):= "010";
CONSTANT D: STD_LOGIC_VECTOR(2 DOWNTO 0):= "011";
CONSTANT E: STD_LOGIC_VECTOR(2 DOWNTO 0):= "100";
CONSTANT F: STD_LOGIC_VECTOR(2 DOWNTO 0):= "101";
CONSTANT G: STD_LOGIC_VECTOR(2 DOWNTO 0):= "110";
CONSTANT H: STD_LOGIC_VECTOR(2 DOWNTO 0):= "111";
SIGNAL morse : STD_LOGIC_VECTOR(3 DOWNTO 0);

BEGIN

PROCESS(letter)
	BEGIN
		CASE letter IS
			WHEN A=>
				countOut <= "001";
				morse <= "0010";
			WHEN B=>
				countOut <= "011";
				morse <= "0001";
			WHEN C=>
				countOut <= "011";
				morse <= "1010";
			WHEN D=>
				countOut <= "010";
				morse <= "0001";
			WHEN E=>
				countOut <= "000";
				morse <= "0000";
			WHEN F=>
				countOut <= "011";
				morse <= "0100";
			WHEN G=>
				countOut <= "010";
				morse <= "0011";
			WHEN H=>
				countOut <= "011";
				morse <= "0000";

			WHEN OTHERS=>
				countOut <= "000";
				morse <= "0000";
		END CASE;
	END PROCESS;
	
	morseOut <= morse;

END Behavior;
