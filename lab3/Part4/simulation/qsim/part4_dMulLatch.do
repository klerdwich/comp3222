onerror {quit -f}
vlib work
vlog -work work part4_dMulLatch.vo
vlog -work work part4_dMulLatch.vt
vsim -novopt -c -t 1ps -L cycloneiv_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.part4_dMulLatch_vlg_vec_tst
vcd file -direction part4_dMulLatch.msim.vcd
vcd add -internal part4_dMulLatch_vlg_vec_tst/*
vcd add -internal part4_dMulLatch_vlg_vec_tst/i1/*
add wave /*
run -all
