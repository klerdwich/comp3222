vlog -work work C:/Users/lerdwichagul/Desktop/UNSW/2014_S2/COMP3222/Labs/lab3/simulation/modelsim/part4.vwf.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.part1_rsLatch_vlg_vec_tst
onerror {resume}
add wave {part1_rsLatch_vlg_vec_tst/i1/x1}
add wave {part1_rsLatch_vlg_vec_tst/i1/x2}
add wave {part1_rsLatch_vlg_vec_tst/i1/f}
run -all
