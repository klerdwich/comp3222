LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY part4_dMulLatch IS
	PORT (Clk, D: IN STD_LOGIC;
	Q : OUT STD_LOGIC);	
END part4_dMulLatch;

ARCHITECTURE Behavior OF part4_dMulLatch IS
BEGIN
	PROCESS (D,Clk)
		BEGIN
		IF (Clk = '1') THEN
			Q <= D;
		END IF;
	END PROCESS;
END Behavior;