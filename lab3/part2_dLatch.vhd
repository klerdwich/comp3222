LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY part2_dLatch IS
	PORT ( SW : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
	LEDR : OUT STD_LOGIC_VECTOR(7 DOWNTO 0));
END part2_dLatch;
ARCHITECTURE Structural OF part2_dLatch IS
	SIGNAL R_g, S_g, Qa, Qb : STD_LOGIC ;
	ATTRIBUTE keep : boolean;
	ATTRIBUTE keep of R_g, S_g, Qa, Qb : SIGNAL IS true;
BEGIN
		R_g <= NOT(NOT (SW(0)) AND SW(1));
		S_g <= NOT( SW(0) AND SW(1));
		Qa <= NOT (S_g AND Qb);
		Qb <= NOT (R_g AND Qa);
		LEDR(0) <= Qa;
END Structural;