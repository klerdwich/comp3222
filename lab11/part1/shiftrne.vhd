LIBRARY ieee; 
 USE ieee.std_logic_1164.all ; 
  ENTITY shiftrne IS 
    PORT (  R      : IN    STD_LOGIC_VECTOR( 7 DOWNTO 0) ; 
        LA,EA, w, Clock   : IN     STD_LOGIC ; 
       Q       : BUFFER  STD_LOGIC_VECTOR( 7 DOWNTO 0) ) ; 
  END shiftrne; 
  ARCHITECTURE Behavior OF shiftrne IS 
BEGIN
    PROCESS (Clock)
    BEGIN
	 IF LA = '1' THEN
			  Q <= R ; 
    ELSIF (Clock'EVENT AND Clock = '1') THEN
			IF(EA = '1') THEN
			  Q(6 DOWNTO 0) <= Q(7 DOWNTO 1); 
			  Q(7) <= w ; 
			END IF ; 
		END IF;
    END PROCESS ; 
END Behavior ;