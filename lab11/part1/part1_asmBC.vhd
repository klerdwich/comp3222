LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_unsigned.all ;

ENTITY part1_asmBC IS
  PORT (  SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    KEY : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
	 LEDG: OUT STD_LOGIC_VECTOR(7 DOWNTO 0));

END part1_asmBC;

ARCHITECTURE Behavior OF part1_asmBC IS

COMPONENT shiftrne
	PORT (  R      : IN    STD_LOGIC_VECTOR( 7 DOWNTO 0) ; 
			  LA,EA, w, Clock   : IN     STD_LOGIC ; 
			 Q       : BUFFER  STD_LOGIC_VECTOR( 7 DOWNTO 0) ) ; 
END COMPONENT;

TYPE State_type IS (S1,S2,S3);
SIGNAL y: State_type;
SIGNAL Resetn,Clock,s,Done,z: STD_LOGIC;
SIGNAL A, B,Data : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL LA,EA,EB,LB,low : STD_LOGIC;

BEGIN

Clock <= KEY(1);
Resetn <= NOT KEY(0);
LA <= NOT KEY(0);
s<= SW(8);
Data<= SW(7 DOWNTO 0);
LEDG(0) <= Done;
LEDR(7 DOWNTO 0) <= B(7 DOWNTO 0);

FSM_transitions: PROCESS(Resetn, Clock)
BEGIN
	IF(Clock'event AND Clock = '1') THEN
	
	CASE y IS
		WHEN S1 =>
			IF s ='0' THEN y <=S1;
			ELSE y <= S2;
			END IF;
		WHEN S2 =>
			IF z = '0' THEN y<= S2;
			ELSE y <= S3;
			END IF;
		WHEN S3 =>
			IF s='0' THEN y<= S1;
			ELSE y<= S3;
			END IF;
			
	END CASE;
	END IF;


END PROCESS;

FSM_outputs: PROCESS(y,A(0))
BEGIN
	EA<='0'; EB<='0'; LB<='0'; Done<='0';
	CASE y IS 
		WHEN S1 =>
			LB<='1';
		WHEN S2 =>
			EA <= '1';
			IF A(0)='1' THEN EB <='1';
			END IF;
		WHEN S3 =>
			Done<='1';
	END CASE;
END PROCESS;


upCount: PROCESS (Resetn, Clock)
BEGIN
	IF Resetn = '1' THEN	
		B<="00000000";
	ELSIF (Clock'EVENT AND Clock = '1') THEN
		IF LB = '1' THEN
			B<="00000000";
		ELSIF EB ='1' THEN
			B<=B+1;
		
		END IF;
		
	
	END IF;
END PROCESS;
  low<='0';
  ShiftA: shiftrne PORT MAP (Data, LA, EA, low, CLOCK, Q=>A);
  z<= '1' WHEN A = "00000000" ELSE '0';
  END Behavior ;


