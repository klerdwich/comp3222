LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_unsigned.all ;

ENTITY part2_sort IS
  PORT (  SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0); --Data, w
    KEY : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Clock, reset
    LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
	 LEDG : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	 N : BUFFER STD_LOGIC_VECTOR(7 DOWNTO 0)); -- Buswires, Done
END part2_sort ;

ARCHITECTURE Behavior OF part2_sort IS

COMPONENT memory_block
	PORT
	(  address		: IN STD_LOGIC_VECTOR (4 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		data		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		wren		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
	);
END COMPONENT;

TYPE State_type IS (S1,S2,S3,S4);
SIGNAL y: State_type;
SIGNAL Resetn,Clock,s,Found,g,z: STD_LOGIC;
SIGNAL  A : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL L,K,M,LPrev : STD_LOGIC_VECTOR(4 DOWNTO 0);
SIGNAL LL,EM,Egt,Elt : STD_LOGIC;
BEGIN

--memoryA: memory_block PORT MAP ("11000",KEY(0),"00000000",'0',q=>LEDR(7 DOWNTO 0));
Clock <= KEY(1);
Resetn <= NOT KEY(0);
s<= SW(8);
A<= SW(7 DOWNTO 0);
M<= "11111";
LEDG(0) <= Found;
LEDR(4 DOWNTO 0) <= L(4 DOWNTO 0);


PROCESS(Resetn, Clock)

BEGIN
	IF(Resetn = '1') THEN
		y<=S1;
	ELSIF(Clock'EVENT AND Clock = '1') THEN
		CASE y IS
		WHEN S1 =>
			IF s ='0' THEN y <=S1;
			ELSE y <= S2;
			END IF;
		WHEN S2 =>
			y<= S3;
		WHEN S3 =>
			IF z = '0' THEN y<= S2;
			ELSE y <= S4;
			END IF;
		WHEN S4 =>
		IF s='0' THEN y<= S1;
				ELSE y<= S4;
				END IF;	
		END CASE;
	END IF;
END PROCESS;
	PROCESS(y)
	BEGIN
		LEDG(5 DOWNTO 2) <= "0000";
		CASE y IS
		WHEN S1 =>
		LEDG(2) <= '1';
		WHEN S2 =>
		LEDG(3) <= '1';
		WHEN S3 =>
		LEDG(4) <= '1';
		WHEN S4 =>
		LEDG(5) <= '1';
		END CASE;
	END PROCESS;
	FSM_outputs: PROCESS(y,L)
BEGIN
	EM<='0'; LL<='0'; Elt <= '0'; Egt <='0';
	CASE y IS 
		WHEN S1 =>
			LL<='1';
			
		WHEN S2 =>

			
		WHEN S3 =>
			EM <= '1';
		WHEN S4 =>
	END CASE;
END PROCESS;
--Data path
getMemory: PROCESS(N, Resetn)
BEGIN
	IF (Resetn = '1') THEN
		L(3 DOWNTO 0) <= M(4 DOWNTO 1);
		L(4)<= '0';
		LPrev <= M;
		K<="00000";
	ELSIF (Clock'EVENT AND Clock = '1') THEN
	IF EM = '1' THEN
		
			IF N = A THEN
				Found<='1';
			ELSIF N< A THEN
					--LPrev+L / 2
					
					K <= Lprev + L;
					L(3 DOWNTO 0) <= K(4 DOWNTO 1);
					L(4) <= '0';
					Found<='0';
					
				ELSIF N > A THEN
					LPrev<=L;
					--divide by 2
					L(3 DOWNTO 0) <= L(4 DOWNTO 1);
					L(4)<= '0';
					Found<='0';
			END IF;
			
		END IF;
	END IF;
END PROCESS;

LEDG(1) <= EM;
memoryA: memory_block PORT MAP (L,Clock,"00000000",'0',q=>N);
z<= '1' WHEN N = A ELSE '0';
			
  
END Behavior ;


