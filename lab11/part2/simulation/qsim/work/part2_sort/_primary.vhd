library verilog;
use verilog.vl_types.all;
entity part2_sort is
    port(
        SW              : in     vl_logic_vector(9 downto 0);
        KEY             : in     vl_logic_vector(3 downto 0);
        LEDR            : out    vl_logic_vector(9 downto 0);
        LEDG            : out    vl_logic_vector(7 downto 0);
        N               : out    vl_logic_vector(7 downto 0)
    );
end part2_sort;
