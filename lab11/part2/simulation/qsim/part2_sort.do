onerror {quit -f}
vlib work
vlog -work work part2_sort.vo
vlog -work work part2_sort.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.part2_sort_vlg_vec_tst
vcd file -direction part2_sort.msim.vcd
vcd add -internal part2_sort_vlg_vec_tst/*
vcd add -internal part2_sort_vlg_vec_tst/i1/*
add wave /*
run -all
