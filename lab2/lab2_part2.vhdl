LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY part2_bcd IS
	PORT( SW: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			HEX0: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			HEX1: OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END part2_bcd;

ARCHITECTURE Structure of part2_bcd IS

COMPONENT adder4
PORT ( Cin : IN STD_LOGIC ;
	x3, x2, x1, x0 : IN STD_LOGIC ;
	y3, y2, y1, y0 : IN STD_LOGIC ;
	s3, s2, s1, s0 : OUT STD_LOGIC ;
	Cout : OUT STD_LOGIC ) ;
END COMPONENT adder4;

COMPONENT fulladd
PORT ( Cin, x, y : IN STD_LOGIC ;
	 Cout, s : OUT STD_LOGIC ) ;
END COMPONENT fulladd;

COMPONENT switch_dec
	PORT( S,U,V,W : IN STD_LOGIC;
			num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));

	END COMPONENT switch_dec;
	
COMPONENT char_7seg
		PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT char_7seg;
	
COMPONENT comparator
	PORT (S, U, V, W: IN STD_LOGIC; 
			Z: OUT STD_LOGIC);
	END COMPONENT comparator;
	
COMPONENT mux_2to1
	PORT (Z,X,Y : IN STD_LOGIC;
				M: OUT STD_LOGIC);
	END COMPONENT mux_2to1;
	
COMPONENT circA
	PORT (V: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			Zin: IN STD_LOGIC;
			M: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			COut: OUT STD_LOGIC);
	END COMPONENT circA;
	
COMPONENT circB
	PORT (ZIn:IN STD_LOGIC;
			dOut:OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
	END COMPONENT circB;
	
SIGNAL ZOut : STD_LOGIC;
SIGNAL circAOut : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL muxOut : STD_LOGIC_VECTOR (3 DOWNTO 0);
SIGNAL num0 :STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL num1 :STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL d1 : STD_LOGIC_VECTOR (3 DOWNTO 0);

BEGIN

	Z0: comparator PORT MAP (SW(3), SW(2), SW(1), SW(0), Z => ZOut);
	CA0: circA PORT MAP (SW(3 DOWNTO 0), ZOut, M=>circAOut);
	
	M0: mux_2to1 PORT MAP (ZOut, SW(0), circAOut(0), M=>muxOut(0));
	M1: mux_2to1 PORT MAP (ZOut, SW(1), circAOut(1), M=>muxOut(1));
	M2: mux_2to1 PORT MAP (ZOut, SW(2), circAOut(2), M=>muxOut(2));
	M3: mux_2to1 PORT MAP (ZOut, SW(3), circAOut(3), M=>muxOut(3));
	
	CB0: circB PORT MAP (ZOut, dOut => d1);
	N0: switch_dec PORT MAP (muxOut(3), muxOut(2), muxOut(1), muxOut(0), num => num0);
	H0: char_7seg PORT MAP (num0, HEXN=>HEX0);
	N1: switch_dec PORT MAP (d1(3), d1(2), d1(1), d1(0), num => num1);
	H1: char_7seg PORT MAP (num1, HEXN=>HEX1);
	
	
END Structure;

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY circB IS
		PORT (ZIn:IN STD_LOGIC;
			dOut:OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END circB;

ARCHITECTURE Behaviour of circB IS

BEGIN
	dOut(0) <= ZIn;
END Behaviour;

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY comparator IS
		PORT (S, U, V, W: IN STD_LOGIC; 
			Z: OUT STD_LOGIC);
END comparator;

ARCHITECTURE Behaviour of comparator IS

BEGIN
	Z <= S AND NOT((NOT U AND NOT V AND NOT W) OR 
		(NOT U AND NOT V AND W));
END Behaviour;

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_2to1 IS
		PORT (Z,X,Y : IN STD_LOGIC;
				M: OUT STD_LOGIC);
END mux_2to1;

ARCHITECTURE Behaviour of mux_2to1 IS

BEGIN
	M <= (NOT Z AND X) OR( Z AND Y);
END Behaviour;

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY circA IS
		PORT (V: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			Zin: IN STD_LOGIC;
			M: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			COut: OUT STD_LOGIC);
END circA;

ARCHITECTURE Behaviour of circA IS
COMPONENT adder4
	PORT ( Cin : IN STD_LOGIC ;
	
	x3, x2, x1, x0 : IN STD_LOGIC ;
	y3, y2, y1, y0 : IN STD_LOGIC ;
	s3, s2, s1, s0 : OUT STD_LOGIC ;
	Cout : OUT STD_LOGIC ) ;
END COMPONENT adder4;

SIGNAL zero: STD_LOGIC;

BEGIN
	zero <= NOT ZIn;
	ADD6: adder4 PORT MAP (zero, V(3), V(2), V(1), V(0), zero, Zin,Zin,zero, s3=>M(3), s2=>M(2), s1=>M(1), s0=>M(0), Cout=>COut); 
END Behaviour;


LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY switch_dec IS
	PORT( S,U,V,W : IN STD_LOGIC;
		   num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));
END switch_dec;

ARCHITECTURE Behaviour of switch_dec IS

BEGIN
	num(0) <= NOT S AND NOT W AND NOT U AND NOT V;
	num(1) <= NOT S AND NOT U AND NOT V AND W;
	num(2) <= NOT S AND NOT U AND V AND NOT W;
	num(3) <= NOT S AND NOT U AND V AND W;
	num(4) <= NOT S AND U AND NOT V AND NOT W;
	num(5) <= NOT S AND U AND NOT V AND W;
	num(6) <= NOT S AND U AND V AND NOT W;
	num(7) <= NOT S AND U AND V AND W;
	num(8) <= S AND NOT U AND NOT V AND NOT W;
	num(9) <= S AND NOT U AND NOT V AND W;
	
END Behaviour;


LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY char_7seg IS
	PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
	HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END char_7seg;

ARCHITECTURE Behaviour of char_7seg IS

BEGIN

HEXN(0) <= number(1) OR number(4);
HEXN(1) <= number(5) OR number (6);
HEXN(2) <= number(2);
HEXN(3) <= number(1) OR number(4) OR number(7);
HEXN(4) <= number(1) OR number(3) OR number(4) OR number(5) OR number(7) OR number(9);
HEXN(5) <= number(1) OR number(2) OR number(3);
HEXN(6) <= number(0) OR number(1) OR number(7);
	
END Behaviour;

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
ENTITY adder4 IS
	PORT ( Cin : IN STD_LOGIC ;
	x3, x2, x1, x0 : IN STD_LOGIC ;
	y3, y2, y1, y0 : IN STD_LOGIC ;
	s3, s2, s1, s0 : OUT STD_LOGIC ;
	Cout : OUT STD_LOGIC ) ;
END adder4;
ARCHITECTURE Structure OF adder4 IS
	SIGNAL c1, c2, c3 : STD_LOGIC ;
	COMPONENT fulladd
	PORT ( Cin, x, y : IN STD_LOGIC ;
		 Cout,s : OUT STD_LOGIC ) ;
END COMPONENT ;

BEGIN
	stage0: fulladd PORT MAP ( Cin, x0, y0, c1, s=>s0 ) ;
	stage1: fulladd PORT MAP ( c1, x1, y1, c2, s=>s1) ;
	stage2: fulladd PORT MAP ( c2, x2, y2, c3, s=>s2 ) ;
	stage3: fulladd PORT MAP (Cin=>c3, Cout=>Cout, x=>x3, y=>y3, s=>s3);
END Structure ;

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
ENTITY fulladd IS
	PORT ( Cin, x, y : IN STD_LOGIC ;
	 Cout, s : OUT STD_LOGIC ) ;
END fulladd ;
ARCHITECTURE LogicFunc OF fulladd IS
	BEGIN
		s<=x XOR y XOR Cin ;
		Cout<=(x AND y) OR (Cin AND x) OR (Cin AND y) ;
END LogicFunc ;