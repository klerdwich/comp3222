LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab2 IS
	PORT( SW: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
			HEX0: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			HEX1: OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END lab2;

ARCHITECTURE Structure of lab2 IS
	SIGNAL num0 :STD_LOGIC_VECTOR(9 DOWNTO 0);
	SIGNAL num1 :STD_LOGIC_VECTOR(9 DOWNTO 0);
COMPONENT switch_dec
	PORT( S,U,V,W : IN STD_LOGIC;
			num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));

	END COMPONENT switch_dec;
	
COMPONENT char_7seg
		PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT char_7seg;
BEGIN

	N0: switch_dec PORT MAP (SW(3), SW(2), SW(1), SW(0), num => num0);
	N1: switch_dec PORT MAP (SW(7), SW(6), SW(5), SW(4), num => num1);
	
	H0: char_7seg PORT MAP (num0, HEXN=>HEX0);
	H1: char_7seg PORT MAP (num1, HEXN=>HEX1);
	
END Structure;

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY switch_dec IS
	PORT( S,U,V,W : IN STD_LOGIC;
		   num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));
END switch_dec;

ARCHITECTURE Behaviour of switch_dec IS

BEGIN
	num(0) <= NOT S AND NOT W AND NOT U AND NOT V;
	num(1) <= NOT S AND NOT U AND NOT V AND W;
	num(2) <= NOT S AND NOT U AND V AND NOT W;
	num(3) <= NOT S AND NOT U AND V AND W;
	num(4) <= NOT S AND U AND NOT V AND NOT W;
	num(5) <= NOT S AND U AND NOT V AND W;
	num(6) <= NOT S AND U AND V AND NOT W;
	num(7) <= NOT S AND U AND V AND W;
	num(8) <= S AND NOT U AND NOT V AND NOT W;
	num(9) <= S AND NOT U AND NOT V AND W;
	
END Behaviour;


LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY char_7seg IS
	PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
	HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END char_7seg;

ARCHITECTURE Behaviour of char_7seg IS

BEGIN

HEXN(0) <= number(1) OR number(4);
HEXN(1) <= number(5) OR number (6);
HEXN(2) <= number(2);
HEXN(3) <= number(1) OR number(4) OR number(7);
HEXN(4) <= number(1) OR number(3) OR number(4) OR number(5) OR number(7) OR number(9);
HEXN(5) <= number(1) OR number(2) OR number(3);
HEXN(6) <= number(0) OR number(1) OR number(7);
	
END Behaviour;