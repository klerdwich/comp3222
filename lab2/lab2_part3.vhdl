LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
ENTITY part3_adder IS
PORT ( Cin : IN STD_LOGIC ;
		SW : IN STD_LOGIC_VECTOR(7 DOWNTO 0) ;
		LEDG : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		LEDR : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		Cout : OUT STD_LOGIC ) ;
END part3_adder;
ARCHITECTURE Structure OF part3_adder IS
	SIGNAL c1, c2, c3 : STD_LOGIC ;
	COMPONENT fulladd
	PORT ( Cin, x, y : IN STD_LOGIC ;
		 Cout,s : OUT STD_LOGIC ) ;
END COMPONENT ;

BEGIN
	LEDR <= SW;
	stage0: fulladd PORT MAP ( Cin, SW(0), SW(4), c1, s=>LEDG(0) ) ;
	stage1: fulladd PORT MAP ( c1, SW(1), SW(5),  c2, s=>LEDG(1)) ;
	stage2: fulladd PORT MAP ( c2, SW(2), SW(6), c3,  s=> LEDG(2) ) ;
	stage3: fulladd PORT MAP (Cin =>c3, Cout =>Cout, x =>SW(3), y=>SW(7), s=>LEDG(3));
END Structure ;

LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
ENTITY fulladd IS
	PORT ( Cin, x, y : IN STD_LOGIC ;
	 Cout, s : OUT STD_LOGIC ) ;
END fulladd ;
ARCHITECTURE LogicFunc OF fulladd IS
	BEGIN
		s<=x XOR y XOR Cin ;
		Cout<=(x AND y) OR (Cin AND x) OR (Cin AND y) ;
END LogicFunc ;