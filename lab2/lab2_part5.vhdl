LIBRARY ieee;
USE ieee.std_logic_1164.all;


ENTITY part5_BCD_if IS
	PORT( SW: IN STD_LOGIC_VECTOR(8 DOWNTO 0);
			LEDG: OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
			LEDR: OUT STD_LOGIC_VECTOR (8 DOWNTO 0);
			HEX0: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			HEX1: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			HEX2: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			HEX3: OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END part5_BCD_if;

ARCHITECTURE Structure of part5_BCD_if IS
COMPONENT switch_dec
	PORT( S,U,V,W : IN STD_LOGIC;
			num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));

	END COMPONENT switch_dec;
	
COMPONENT char_7seg
		PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT char_7seg;
	
COMPONENT adder4
	PORT ( Cin : IN STD_LOGIC ;
		x3, x2, x1, x0 : IN STD_LOGIC ;
		y3, y2, y1, y0 : IN STD_LOGIC ;
		s3, s2, s1, s0 : OUT STD_LOGIC ;
		Cout : OUT STD_LOGIC ) ;
END COMPONENT adder4;	

	COMPONENT sum
	PORT( CIn: IN STD_LOGIC;
			A: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			B: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		   S1: OUT STD_LOGIC;
			S0: OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
	END COMPONENT sum;
	
	SIGNAL num0 :STD_LOGIC_VECTOR(9 DOWNTO 0);
	SIGNAL num1 :STD_LOGIC_VECTOR(9 DOWNTO 0);
	SIGNAL S1: STD_LOGIC;
	SIGNAL num2 :STD_LOGIC_VECTOR(9 DOWNTO 0);
	SIGNAL num3 :STD_LOGIC_VECTOR(9 DOWNTO 0);
	SIGNAL S0: STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN

	LEDR <= SW;

	S: sum PORT MAP (SW(8), SW(7 DOWNTO 4), SW(3 DOWNTO 0),S1=> S1,S0=> S0 );
	
	N0: switch_dec PORT MAP (S0(3), S0(2), S0(1), S0(0), num => num0);
	N1: switch_dec PORT MAP ('0', '0', '0', S1, num => num1);
	
	H0: char_7seg PORT MAP (num0, HEXN=>HEX0);
	H1: char_7seg PORT MAP (num1, HEXN=>HEX1);
	
	N2: switch_dec PORT MAP (SW(3), SW(2), SW(1), SW(0), num => num2);
	N3: switch_dec PORT MAP (SW(7), SW(6), SW(5), SW(4), num => num3);
	
	H2: char_7seg PORT MAP (num2, HEXN=>HEX2);
	H3: char_7seg PORT MAP (num3, HEXN=>HEX3);

	
END Structure;

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all ;

ENTITY sum IS
	PORT( CIn: IN STD_LOGIC;
			A: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			B: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		   S1: OUT STD_LOGIC;
			S0: OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END sum;

ARCHITECTURE Behaviour of sum IS
SIGNAL COut: STD_LOGIC;
SIGNAL total: STD_LOGIC_VECTOR(4 DOWNTO 0);
SIGNAL Z: STD_LOGIC_VECTOR (3 DOWNTO 0);
SIGNAL result: STD_LOGIC_VECTOR(4 DOWNTO 0);
	
BEGIN

total <= ('0'&A) + ('0'&B) + CIn;
PROCESS(total)
BEGIN
	IF (total > 9) THEN
		Z <= "1010";
		COut <= '1';
	ELSE
		Z <= "0000";
		COut <= '0';
	END IF;
END PROCESS;
result <= total - Z;
s0<=result(3 DOWNTO 0);
S1 <= COut;

END Behaviour;
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY switch_dec IS
	PORT( S,U,V,W : IN STD_LOGIC;
		   num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));
END switch_dec;

ARCHITECTURE Behaviour of switch_dec IS

BEGIN
	num(0) <= NOT S AND NOT W AND NOT U AND NOT V;
	num(1) <= NOT S AND NOT U AND NOT V AND W;
	num(2) <= NOT S AND NOT U AND V AND NOT W;
	num(3) <= NOT S AND NOT U AND V AND W;
	num(4) <= NOT S AND U AND NOT V AND NOT W;
	num(5) <= NOT S AND U AND NOT V AND W;
	num(6) <= NOT S AND U AND V AND NOT W;
	num(7) <= NOT S AND U AND V AND W;
	num(8) <= S AND NOT U AND NOT V AND NOT W;
	num(9) <= S AND NOT U AND NOT V AND W;
	
END Behaviour;


LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY char_7seg IS
	PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
	HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END char_7seg;

ARCHITECTURE Behaviour of char_7seg IS

BEGIN

HEXN(0) <= number(1) OR number(4);
HEXN(1) <= number(5) OR number (6);
HEXN(2) <= number(2);
HEXN(3) <= number(1) OR number(4) OR number(7);
HEXN(4) <= number(1) OR number(3) OR number(4) OR number(5) OR number(7) OR number(9);
HEXN(5) <= number(1) OR number(2) OR number(3);
HEXN(6) <= number(0) OR number(1) OR number(7);
	
END Behaviour;