library verilog;
use verilog.vl_types.all;
entity part1_bj is
    port(
        clk             : in     vl_logic;
        sw_clk          : in     vl_logic;
        start           : in     vl_logic;
        cardValue       : in     vl_logic_vector(3 downto 0);
        cardReady       : in     vl_logic;
        newCard         : out    vl_logic;
        lost            : out    vl_logic;
        finished        : out    vl_logic;
        score           : out    vl_logic_vector(4 downto 0);
        data7s          : out    vl_logic_vector(0 to 7);
        addr7s          : out    vl_logic_vector(3 downto 0);
        staten          : out    vl_logic_vector(3 downto 0);
        totalVal        : out    vl_logic_vector(5 downto 0)
    );
end part1_bj;
