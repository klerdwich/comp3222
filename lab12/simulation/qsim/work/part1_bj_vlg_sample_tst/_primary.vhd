library verilog;
use verilog.vl_types.all;
entity part1_bj_vlg_sample_tst is
    port(
        cardReady       : in     vl_logic;
        cardValue       : in     vl_logic_vector(3 downto 0);
        clk             : in     vl_logic;
        start           : in     vl_logic;
        sw_clk          : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end part1_bj_vlg_sample_tst;
