----------------------------------------------------------------------------------
-- Company: UNSW
-- Engineer: Jorgen Peddersen
-- 
-- Create Date:    16:06:48 09/26/2006 
-- Design Name:    Blackjack Player
-- Module Name:    Blackjack Datapath - Structural 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

entity Blackjack_DataPath is
  
  port (
    clk                       : in  std_logic;
    rst                       : in  std_logic;
    cardValue                 : in  std_logic_vector(3 downto 0);
    score                     : out std_logic_vector(4 downto 0);
    sel                       : in  std_logic;
    enaLoad, enaAdd, enaScore : in  std_logic;
    cmp11, cmp16, cmp21       : out std_logic;
	 totalVal : buffer STD_LOGIC_vector(5 DOWNTO 0));

end Blackjack_DataPath;

architecture Structural of Blackjack_DataPath is

SIGNAL myCard : STD_logic_vector(5 DOWNTO 0);

begin  -- Structural

datapath: PROCESS (rst,clk) 
BEGIN

IF(rst = '1') THEN
	myCard <= "000000";
	totalVal <= "000000";
ELSIF (clk'event AND clk = '1') THEN
	IF(enaLoad = '1') THEN
		IF(sel = '1') THEN --mux
		myCard <= "00" & cardValue;
		ELSIF (sel = '0') THEN
			myCard <= "110110"; -- -10
		END IF;
	END IF;
	IF(enaAdd= '1') THEN
		totalVal <= totalVal +  myCard;
	END IF;
END IF;

END PROCESS datapath;

compares: PROCESS(rst,totalVal,clk)
BEGIN
	IF(rst = '1') THEN
		cmp11<='0';
		cmp16<='0';
		cmp21<='0';
	ELSE
		IF (myCard = 11) THEN
			cmp11 <= '1';
		ELSE 
			cmp11 <= '0';
		END IF;
		IF(totalVal > 21) THEN
			cmp21<='1';
			ELSE
			cmp21<='0';
		END IF;
		IF(totalVal > 16) THEN
			cmp16 <= '1';
			ELSE
			cmp16 <='0';
		END IF;

	END IF;
END PROCESS compares;

  

end Structural;
