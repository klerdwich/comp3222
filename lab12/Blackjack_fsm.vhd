----------------------------------------------------------------------------------
-- Company: UNSW
-- Engineer: Jorgen Peddersen
-- 
-- Create Date:    16:06:48 09/26/2006 
-- Design Name:    Blackjack Player
-- Module Name:    Blackjack FSM - Behavioural 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Blackjack_FSM is
  
  port (
    clk                       : in  std_logic;
    rst                       : in  std_logic;
    cardReady                 : in  std_logic;
    newCard                   : out std_logic;
    lost                      : out std_logic;
    finished                  : out std_logic;
    cmp11, cmp16, cmp21       : in  std_logic;
    sel                       : out std_logic;
    enaLoad, enaAdd, enaScore : out std_logic;
	 staten : out std_logic_vector(3 DOWNTO 0));

end Blackjack_FSM;


architecture Behavioural of Blackjack_FSM is
Type State_type IS (start,load,score,lostState,finishedState,newCardState,addState);
SIGNAL y : State_type;
begin  -- Behavioural

PROCESS (rst, clk) 
BEGIN


IF(rst = '1') THEN
enaLoad<='0';
enaAdd<='0';
enaScore<='0';
newCard <= '1';
lost <= '0';
finished <='0';
y<=start;
ELSIF (clk'event AND clk = '1') THEN
	CASE y IS
	
	WHEN start =>
		staten <= "0001";
		IF(cardReady = '0') THEN
			y<=start;
		ELSE
			enaLoad <='1';
			sel <= '1';
			y<=load;
		END IF;
		
	WHEN load =>
	   staten <= "0010";
		newCard <= '0';
		enaLoad <= '0';
		
		enaAdd <= '1';
		y<=addState;
	WHEN addState =>
	staten <= "1000";
	enaAdd <= '0';
	y<=score; -- this is so compares can now be performed.
	WHEN score =>
	staten <= "0011";
		
		IF(cmp21 = '1' AND cmp11 = '1') THEN
			sel <= '0'; --subtract 10
			enaLoad <= '1';
			y<=load;
		
		ELSIF(cmp21 = '1' AND cmp11 = '0') THEN
			y<=lostState;
		ELSIF (cmp21='0' AND cmp16 ='1') THEN
			y<=finishedState;
		ELSIF (cmp16 = '0' AND cmp21 ='0') THEN
			y<= newCardState;
		END IF;
	WHEN newCardState =>
	staten <= "0100";
		newCard <= '1';
		
		IF(cardReady = '0') THEN
			y<= newCardState;
		ELSE 
			enaLoad<='1';
			sel <= '1';
			y<= load;
		END IF;
	WHEN finishedState =>
	   staten <= "0101";
		finished <= '1';
	WHEN lostState =>
	staten <= "0110";
		lost <='1';
	END CASE;
END IF;

END PROCESS;
  

end Behavioural;
