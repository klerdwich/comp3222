
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity part1_bj is
    Port (  SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0); --Data, w
    KEY : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Clock, reset
    LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
	 LEDG : OUT STD_LOGIC_VECTOR(7 DOWNTO 0));
end part1_bj;

architecture Structural of part1_bj is

component Blackjack
    Port ( clk : in  STD_LOGIC;     -- on-board clock - fast
           sw_clk : in STD_LOGIC;   -- switch controlled clock - slow
           start : in  STD_LOGIC;   -- start input (asynchronous reset)
           cardValue : in  STD_LOGIC_VECTOR (3 downto 0);
           cardReady : in  STD_LOGIC;
           newCard : out  STD_LOGIC;
           lost : out  STD_LOGIC;
           finished : out  STD_LOGIC;
           score : out  STD_LOGIC_VECTOR (4 downto 0);
           data7s : out  STD_LOGIC_VECTOR (0 to 7);
           addr7s : out  STD_LOGIC_VECTOR (3 downto 0));
end component;
  
begin

bj: Blackjack PORT MAP (SW(9) => sw_clk, KEY(0)=>start, SW(3 DOWNTO 0) => cardValue(3 DOWNTO 0), SW(4) => cardReady, newCard=>LEDG(5), lost=>LEDG(6),finished=> LEDG(7),score(4 DOWNTO 0) => LEDR(4 DOWNTO 0));

 
end Structural;

