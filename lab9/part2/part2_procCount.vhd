LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_signed.all ;
USE work.subccts.all;

ENTITY part2_procCount IS
  PORT (  SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0); --Data, w
    KEY : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Clock, reset
    LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
	 LEDG : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)); -- Buswires, Done
END part2_procCount ;

ARCHITECTURE Behavior OF part2_procCount IS

COMPONENT proc
	PORT (  Data      : IN    STD_LOGIC_VECTOR(8 DOWNTO 0) ;
    Reset, w    : IN    STD_LOGIC ;
    Clock     : IN    STD_LOGIC ;
    F, Rx, Ry   : IN    STD_LOGIC_VECTOR(2 DOWNTO 0) ;
    Done      : BUFFER  STD_LOGIC ;
    BusWires  : BUFFER   STD_LOGIC_VECTOR(8 DOWNTO 0) ) ;
END COMPONENT;

SIGNAL reset,MClock,PClock: STD_LOGIC;
SIGNAL Counter: STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL data: STD_LOGIC_VECTOR(8 DOWNTO 0);

BEGIN

reset <= NOT KEY(0);
MClock <= KEY(1);
PClock <= KEY(2);
LEDG(3 DOWNTO 0)<= Counter;
count: upcountN PORT MAP (reset,MClock,Counter);

mem0: Memory PORT MAP (Counter,data,MClock);

simpleProc: proc PORT MAP (Data=>data(8 DOWNTO 0), Reset=>reset, w=>SW(9), Clock=>PClock,F=>data(8 DOWNTO 6),Rx=>data(5 DOWNTO 3),Ry=>data(2 DOWNTO 0),Done=>LEDR(9), BusWires=>LEDR(8 DOWNTO 0));

END Behavior ;


