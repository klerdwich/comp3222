Circuit (top entity) changes:
After each clock pulse, fetch data from the memory and put into the processor through F,Rx and Ry
Increment the PC, turn off increment pc bit untill done has been asserted.

Memory changes:
make the memory block 128 bits wide, with an input access counter of 7 bits. The memory will go to the address stored at the address and return it's contents.
when write enable bit is on, look at data and address and just store data to address. When it is off, access the specified address and output to Din.


Processor changes:
Implement r7: R7out will need to be wired up to the ADDR register, R7 wiull need to increment its contents on the clock pulse if the incr_pc is enabled
For each clock pulse, fetch data and increase the PC if the done bit has been asserted:
Transfer R7 contents to ADDR, ADDR is provided to memory, memory sends data at that address through RIN.
proc:

fetchData: regn PORT MAP ( R7, Done, Clock, ADDR ) ; --if done has been asserted, copy r7 to addr
		   trin PORT MAP (ADDR, done,BusWires)
regn PORT MAP (Func,Extern,Clock,)--read data in as function data... we 


Memory:
--get data from BusWires and look them up in the memory
--load data back to buswires through DIN


Din 

Implement ADDR register: ADDRin will be hooked up to the controller(ADDRIn) OR r7Out
Implement Dout register and write enable bit: Store Doutin into DOut register then on clock signal, send addrout register, dOutout register and write enable bit to the memory.


