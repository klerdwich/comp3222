LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_unsigned.all ;

ENTITY upcountPC IS
   GENERIC ( N : INTEGER := 9 ) ; -- specify number of instructions
	PORT (Clear,Clock : IN STD_LOGIC;
			Q : BUFFER STD_LOGIC_VECTOR(N-1 DOWNTO 0));

END upcountPC ;

ARCHITECTURE Behavior OF upcountPC IS
BEGIN
  upcount: PROCESS ( Clock )
  BEGIN
    IF (Clock'EVENT AND Clock = '1') THEN
        IF Clear = '1' THEN
        ELSE
            Q <= Q + '1' ;
      END IF ;
    END IF;
  END PROCESS;
END Behavior ;

