LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_unsigned.all ;
use IEEE.NUMERIC_STD.all;

ENTITY Memory IS
	GENERIC ( N : INTEGER := 7 ) ; --word size in bits
	PORT (Address: IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
			Data : BUFFER STD_LOGIC_VECTOR(8 DOWNTO 0);
			Clock : IN STD_LOGIC);
END Memory;

ARCHITECTURE Behaviour OF Memory IS
type rom_array is array (0 to 127) of STD_LOGIC_VECTOR(8 DOWNTO 0);
constant rom: rom_array := ("001110000","000000101","001101000","000000110","010110101", OTHERS=>"000000000");

BEGIN
PROCESS ( Clock )
  BEGIN
    IF (Clock'EVENT AND Clock = '1') THEN
		data<= rom(to_integer(unsigned(Address)));
    END IF;
  END PROCESS;

END Behaviour;