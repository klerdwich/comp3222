LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_signed.all ;

ENTITY part1_proc IS
  PORT (  SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0); --Data, w
    KEY : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Clock, reset
    LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)); -- Buswires, Done
END part1_proc ;

ARCHITECTURE Behavior OF part1_proc IS

COMPONENT proc
	PORT (  Data      : IN    STD_LOGIC_VECTOR(8 DOWNTO 0) ;
    Reset, w    : IN    STD_LOGIC ;
    Clock     : IN    STD_LOGIC ;
    F, Rx, Ry   : IN    STD_LOGIC_VECTOR(2 DOWNTO 0) ;
    Done      : BUFFER  STD_LOGIC ;
    BusWires  : BUFFER   STD_LOGIC_VECTOR(8 DOWNTO 0) ) ;
END COMPONENT;

SIGNAL reset: STD_LOGIC;

BEGIN
reset <= NOT KEY(0);

simpleProc: proc PORT MAP (Data=>SW(8 DOWNTO 0), Reset=>reset, w=>SW(9), Clock=>KEY(1),F=>SW(8 DOWNTO 6),Rx=>SW(5 DOWNTO 3),Ry=>SW(2 DOWNTO 0),Done=>LEDR(9), BusWires=>LEDR(8 DOWNTO 0));
  
END Behavior ;


