LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

ENTITY triPC IS
	GENERIC ( N : INTEGER := 9 ) ;
	PORT (	X 	: IN 	STD_LOGIC_VECTOR(N-1 DOWNTO 0) ;
			E	: IN 	STD_LOGIC ;
			F	: OUT 	STD_LOGIC_VECTOR(N-1 DOWNTO 0) ) ;
END triPC ;

ARCHITECTURE Behavior OF triPC IS	
BEGIN
	Process(E)
	BEGIN
		IF(E = '1') THEN
			F<=X;
		END IF;
	END PROCESS;
END Behavior ;

