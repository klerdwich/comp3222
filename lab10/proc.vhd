LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_signed.all ;
USE work.subccts.all ; --include sub components specified in other vhd file

ENTITY proc IS
  PORT (  Data      : IN    STD_LOGIC_VECTOR(8 DOWNTO 0) ;
    Reset, w    : IN    STD_LOGIC ;
    Clock     : IN    STD_LOGIC ;
    F, Rx, Ry   : IN    STD_LOGIC_VECTOR(2 DOWNTO 0) ;
    Done      : BUFFER  STD_LOGIC ;
    BusWires  : BUFFER   STD_LOGIC_VECTOR(8 DOWNTO 0);
	 Addr : OUT STD_LOGIC_VECTOR(8 DOWNTO 0) ) ;
END proc ;

ARCHITECTURE Behavior OF proc IS
  SIGNAL Rin, Rout : STD_LOGIC_VECTOR(0 TO 7) ;
  SIGNAL Clear, High, AddSub,incr_PC,AddrIn : STD_LOGIC ;
  SIGNAL Extern, Ain, Gin, Gout, FRin : STD_LOGIC ;
  SIGNAL Count : STD_LOGIC_VECTOR(1 DOWNTO 0) ;
  SIGNAL I, X, Y : STD_LOGIC_VECTOR(0 TO 7) ;
  SIGNAL T : STD_LOGIC_VECTOR (0 TO 3);
  SIGNAL R0, R1, R2, R3,R4,R5,R6,R7 : STD_LOGIC_VECTOR(8 DOWNTO 0) ;
  SIGNAL A, Sum, G : STD_LOGIC_VECTOR(8 DOWNTO 0) ;
  SIGNAL Func, FuncReg : STD_LOGIC_VECTOR(1 TO 9) ;
BEGIN
  High <= '1' ; 
  Clear <= Reset OR Done OR (NOT w AND T(0)) ;
  counter: upcount PORT MAP ( Clear, Clock, Count ) ;
  decT: dec2to4 PORT MAP ( Count, High, T ); --T is output of what timestep we are in.
  Func <= F & Rx & Ry ;
  
   --Need to add when reading instruction from clock cycle
  FRin <= w AND T(0) ; --if we are in time step 0

  --Because addr and memory use clock signals, memory is only being loaded in Time step 1...
  tri_memory: trin PORT MAP (R7, AddrIn, BusWires); --put address on buswires whenever in timestep 0
  reg_memory: regn PORT MAP (BusWires, AddrIn, Clock, Addr);
 
  upPC: upcountPC PORT MAP (incr_PC, Clock, R7); -- increment R7 whenever incr_PC is enabled
  
--NOTE: FuncReg = IR
  functionreg: regn GENERIC MAP ( N => 9 ) 
        PORT MAP ( Func, FRin, Clock, FuncReg ) ; --wait till clock signal then map Func to FuncReg (only if in T(0)!!)
  decI: dec3to8 PORT MAP ( FuncReg(1 TO 3), High, I ) ; --dec2to4 converts 2 bits into a 'one hot' style of coding
  decX: dec3to8 PORT MAP ( FuncReg(4 TO 6),  High, X ) ;--this way the I,X,Y can be accessed as with indicies. e.g I(0) etc
  decY: dec3to8 PORT MAP ( FuncReg(7 TO 9), High, Y ) ;

  incr_PC <= T(0) OR Done;
  AddrIn <= T(0);
  Extern <= I(1) AND T(1) ;
  Done <= ((I(0) OR I(1)) AND T(1)) OR ((I(2) OR I(3)) AND T(3)) ;
  Ain <= (I(2) OR I(3)) AND T(1) ;
  Gin <= (I(2) OR I(3)) AND T(2) ;
  Gout <= (I(2) OR I(3)) AND T(3) ;
  AddSub <= I(3) ;
  RegCntl:
  FOR k IN 0 TO 7 GENERATE --iterate through the registers and check if they need to be set as Rin or Rout
    Rin(k) <= ((I(0) OR I(1)) AND T(1) AND X(k)) OR
      ((I(2) OR I(3)) AND T(3) AND X(k)) ;
    Rout(k) <= (I(0) AND T(1) AND Y(k)) OR 
      ((I(2) OR I(3)) AND ((T(1) AND X(k)) OR (T(2) AND Y(k)))) ;
  END GENERATE RegCntl ;
  tri_extern: trin PORT MAP ( Data, Extern, BusWires ) ; --copy from data to buswires when extern is enabled
  
  reg0: regn PORT MAP ( BusWires, Rin(0), Clock, R0 ) ; --copy from buswires to the specified register if
  reg1: regn PORT MAP ( BusWires, Rin(1), Clock, R1 ) ; --its Rin is set and we're ona  clock signal
  reg2: regn PORT MAP ( BusWires, Rin(2), Clock, R2 ) ;
  reg3: regn PORT MAP ( BusWires, Rin(3), Clock, R3 ) ;
  reg4: regn PORT MAP ( BusWires, Rin(4), Clock, R4 ) ;
  reg5: regn PORT MAP ( BusWires, Rin(5), Clock, R5 ) ;
  reg6: regn PORT MAP ( BusWires, Rin(6), Clock, R6 ) ;
 -- reg7: regn PORT MAP ( BusWires, Rin(7), Clock, R7 ) ;
  
  tri0: trin PORT MAP ( R0, Rout(0), BusWires ) ; --copy from r0-r7 to bus wires if its Rout is set
  tri1: trin PORT MAP ( R1, Rout(1), BusWires ) ;
  tri2: trin PORT MAP ( R2, Rout(2), BusWires ) ;
  tri3: trin PORT MAP ( R3, Rout(3), BusWires ) ;
  tri4: trin PORT MAP ( R4, Rout(4), BusWires ) ; 
  tri5: trin PORT MAP ( R5, Rout(5), BusWires ) ;
  tri6: trin PORT MAP ( R6, Rout(6), BusWires ) ;
 -- tri7: trin PORT MAP ( R7, Rout(7), BusWires ) ;
  
  regA: regn PORT MAP ( BusWires, Ain, Clock, A ) ; --copy from Buswires to A if Ain is set
  alu:
  WITH AddSub SELECT
    Sum <= A + BusWires WHEN '0',
           A - BusWires WHEN OTHERS ;
  regG: regn PORT MAP ( Sum, Gin, Clock, G ) ; --store the sum temporarily in G after the addition/subtraction
  triG: trin PORT MAP ( G, Gout, BusWires ) ; --when we're in the 3rd time step, set G to G out.
END Behavior ;


