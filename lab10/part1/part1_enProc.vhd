LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_signed.all ;
USE work.subccts.all;

ENTITY part1_enProc IS
  PORT (  SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0); --Data, w
    KEY : IN STD_LOGIC_VECTOR(3 DOWNTO 0); --Clock, reset
    LEDR : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
	 LEDG : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)); -- Buswires, Done
END part1_enProc ;

ARCHITECTURE Behavior OF part1_enProc IS

COMPONENT proc
	PORT (  Data      : IN    STD_LOGIC_VECTOR(8 DOWNTO 0) ;
    Reset, w    : IN    STD_LOGIC ;
    Clock     : IN    STD_LOGIC ;
    F, Rx, Ry   : IN    STD_LOGIC_VECTOR(2 DOWNTO 0) ;
    Done      : BUFFER  STD_LOGIC ;
    BusWires  : BUFFER   STD_LOGIC_VECTOR(8 DOWNTO 0);
	 Addr : OUT STD_LOGIC_VECTOR(8 DOWNTO 0)) ;
END COMPONENT; 

SIGNAL reset,clock: STD_LOGIC;
SIGNAL Counter: STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL data,buswires,address: STD_LOGIC_VECTOR(8 DOWNTO 0);

BEGIN

reset <= NOT KEY(0);
clock <= NOT KEY(1);

mem0: Memory PORT MAP (address(6 DOWNTO 0),data,clock); --input buswire value, get out its corresponding data TODO: add a write enable bit
LEDR(8 DOWNTO 0) <= buswires;
LEDG(6 DOWNTO 0) <= address(6 DOWNTO 0);

simpleProc: proc PORT MAP (Data=>data(8 DOWNTO 0), Reset=>reset, w=>SW(9), Clock=>clock,F=>data(8 DOWNTO 6),Rx=>data(5 DOWNTO 3),Ry=>data(2 DOWNTO 0),Done=>LEDR(9), BusWires=>buswires, Addr => address);

END Behavior ;


