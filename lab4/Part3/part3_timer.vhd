LIBRARY ieee; 
USE ieee.std_logic_1164.all ; 
USE ieee.std_logic_unsigned.all; 
ENTITY part3_timer IS
	PORT (
				CLOCK_50 : IN STD_LOGIC;
				SW : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
				HEX0: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
				LEDG: OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
END part3_timer; 


ARCHITECTURE Behavior OF part3_timer IS

COMPONENT switch_dec
	PORT( S,U,V,W : IN STD_LOGIC;
			num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));

	END COMPONENT switch_dec;
	
COMPONENT char_7seg
		PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT char_7seg;
	
	
	SIGNAL num0 :STD_LOGIC_VECTOR(9 DOWNTO 0);
	SIGNAL timeCounter:STD_LOGIC_VECTOR(25 DOWNTO 0);
	SIGNAL counter: STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN
	process (CLOCK_50) 
   begin
		
		if (CLOCK_50'event and CLOCK_50='1') then
			if (timeCounter >= 50000000) then
				counter <= counter + 1;
				timeCounter <= "00000000000000000000000000";
				if(counter >= 9) then
					counter <= "0000";
				end if;
			else
				timeCounter <= timeCounter+1;
			end if;
		end if;
   end process;
	N0: switch_dec PORT MAP (counter(3),counter(2),counter(1),counter(0), num => num0);
	H0: char_7seg PORT MAP (num0, HEXN=>HEX0);
END Behavior ; 


LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY switch_dec IS
	PORT( S,U,V,W : IN STD_LOGIC;
		   num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));
END switch_dec;

ARCHITECTURE Behaviour of switch_dec IS

BEGIN
	num(0) <= NOT S AND NOT W AND NOT U AND NOT V;
	num(1) <= NOT S AND NOT U AND NOT V AND W;
	num(2) <= NOT S AND NOT U AND V AND NOT W;
	num(3) <= NOT S AND NOT U AND V AND W;
	num(4) <= NOT S AND U AND NOT V AND NOT W;
	num(5) <= NOT S AND U AND NOT V AND W;
	num(6) <= NOT S AND U AND V AND NOT W;
	num(7) <= NOT S AND U AND V AND W;
	num(8) <= S AND NOT U AND NOT V AND NOT W;
	num(9) <= S AND NOT U AND NOT V AND W;
	
END Behaviour;


LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY char_7seg IS
	PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
	HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END char_7seg;

ARCHITECTURE Behaviour of char_7seg IS

BEGIN

HEXN(0) <= number(1) OR number(4);
HEXN(1) <= number(5) OR number (6);
HEXN(2) <= number(2);
HEXN(3) <= number(1) OR number(4) OR number(7);
HEXN(4) <= number(1) OR number(3) OR number(4) OR number(5) OR number(7) OR number(9);
HEXN(5) <= number(1) OR number(2) OR number(3);
HEXN(6) <= number(0) OR number(1) OR number(7);
	
END Behaviour;