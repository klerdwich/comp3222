LIBRARY ieee; 
USE ieee.std_logic_1164.all ; 
USE ieee.std_logic_unsigned.all; 
ENTITY part1_TCounter IS
	PORT (   SW : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
				KEY : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
				HEX0: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
				HEX1: OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END part1_TCounter; 


ARCHITECTURE Behavior OF part1_TCounter IS
SIGNAL temp: STD_LOGIC_VECTOR(7 DOWNTO 0);
COMPONENT T_FF
   PORT( T: in  std_logic;
         clr: in std_logic;
         enable: in std_logic;
         clk: in std_logic;
         Q: out std_logic);
END COMPONENT T_FF;


COMPONENT switch_dec
	PORT( S,U,V,W : IN STD_LOGIC;
			num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));

	END COMPONENT switch_dec;
	
COMPONENT eightBitBCD
	PORT(SW: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			tensOut:OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			digitOut:OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END COMPONENT eightBitBCD;
	
COMPONENT char_7seg
		PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT char_7seg;
	
	
	SIGNAL num0 :STD_LOGIC_VECTOR(9 DOWNTO 0);
	SIGNAL num1 :STD_LOGIC_VECTOR(9 DOWNTO 0);
	SIGNAL tens: STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL digits: STD_LOGIC_VECTOR(3 DOWNTO 0);

BEGIN
	T0: T_FF PORT MAP (temp(0), SW(0),SW(1),KEY(0), Q=>temp(0));
	T1: T_FF PORT MAP (temp(1), SW(0),SW(1) AND temp(0),KEY(0), Q=>temp(1));
	T2: T_FF PORT MAP (temp(2), SW(0),SW(1) AND temp(0) AND temp(1),KEY(0), Q=>temp(2));
	T3: T_FF PORT MAP (temp(3), SW(0),SW(1) AND temp(0) AND temp(1) AND temp(2),KEY(0), Q=>temp(3));
	T4: T_FF PORT MAP (temp(4), SW(0),SW(1) AND temp(0) AND temp(1) AND temp(2)AND temp(3),KEY(0), Q=>temp(4));
	T5: T_FF PORT MAP (temp(5), SW(0),SW(1) AND temp(0) AND temp(1) AND temp(2)AND temp(3) AND temp(4),KEY(0), Q=>temp(5));
	T6: T_FF PORT MAP (temp(6), SW(0),SW(1) AND temp(0) AND temp(1) AND temp(2)AND temp(3) AND temp(4) AND temp(5),KEY(0), Q=>temp(6));
	T7: T_FF PORT MAP (temp(7), SW(0),SW(1) AND temp(0) AND temp(1) AND temp(2)AND temp(3) AND temp(4) AND temp(5) AND temp(6),KEY(0), Q=>temp(7));
	
		
	A: eightBitBCD PORT MAP (temp, tensOut => tens, digitOut => digits);
	N0: switch_dec PORT MAP (digits(3),digits(2),digits(1),digits(0), num => num0);
	N1: switch_dec PORT MAP (tens(3),tens(2),tens(1),tens(0), num => num1);
	H0: char_7seg PORT MAP (num0, HEXN=>HEX0);
	H1: char_7seg PORT MAP (num1, HEXN=>HEX1);
END Behavior ; 


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
 
ENTITY T_FF IS
   port( T: in  std_logic;
         clr: in std_logic;
         enable: in std_logic;
         clk: in std_logic;
         Q: out std_logic);
end T_FF;
 
architecture Behavioral of T_FF is
begin
   process (clk,T,clr,enable) 
   begin
      if (clk'event and clk='1') then 		
         if clr='1' then   
            Q <= '0';
         elsif (enable ='1') then
				Q<= NOT T;
			else
				Q<= T;
         end if;
      end if;
   end process;
end Behavioral;



LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY switch_dec IS
	PORT( S,U,V,W : IN STD_LOGIC;
		   num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));
END switch_dec;

ARCHITECTURE Behaviour of switch_dec IS

BEGIN
	num(0) <= NOT S AND NOT W AND NOT U AND NOT V;
	num(1) <= NOT S AND NOT U AND NOT V AND W;
	num(2) <= NOT S AND NOT U AND V AND NOT W;
	num(3) <= NOT S AND NOT U AND V AND W;
	num(4) <= NOT S AND U AND NOT V AND NOT W;
	num(5) <= NOT S AND U AND NOT V AND W;
	num(6) <= NOT S AND U AND V AND NOT W;
	num(7) <= NOT S AND U AND V AND W;
	num(8) <= S AND NOT U AND NOT V AND NOT W;
	num(9) <= S AND NOT U AND NOT V AND W;
	
END Behaviour;


LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;

ENTITY eightBitBCD IS
	PORT(SW: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			tensOut:OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			digitOut:OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END eightBitBCD;

ARCHITECTURE Behaviour OF eightBitBCD IS
SIGNAL total : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL tens : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL digits : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL result : STD_LOGIC_VECTOR(7 DOWNTO 0);
BEGIN
total <= SW;
PROCESS(total,tens,digits)
BEGIN
	IF (total >= 90) THEN
		tens <= "01011010";
		digits <= "1001";
	ELSIF (total >= 80) THEN
		tens <= "01010000";
		digits <= "1000";
	ELSIF (total >= 70) THEN
		tens <= "01000110";
		digits <= "0111";
	ELSIF (total >= 60) THEN
		tens <= "00111100";
		digits <= "0110";
	ELSIF (total >= 50) THEN
		tens <= "00110010";
		digits <= "0101";
	ELSIF (total >= 40) THEN
		tens <= "00101000";
		digits <= "0100";
	ELSIF (total >= 30) THEN
		tens <= "00011110";
		digits <= "0011";
	ELSIF (total >= 20) THEN
		tens <= "00010100";
		digits <= "0010";
	ELSIF (total >= 10) THEN
		tens <= "00001010";
		digits <= "0001";
	ELSE
		tens <= "00000000";
		digits <= "0000";
	END IF;
END PROCESS;

result <= total - tens;
digitOut <= result(3 DOWNTO 0);
tensOut <= digits;

END Behaviour;





LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY char_7seg IS
	PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
	HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END char_7seg;

ARCHITECTURE Behaviour of char_7seg IS

BEGIN

HEXN(0) <= number(1) OR number(4);
HEXN(1) <= number(5) OR number (6);
HEXN(2) <= number(2);
HEXN(3) <= number(1) OR number(4) OR number(7);
HEXN(4) <= number(1) OR number(3) OR number(4) OR number(5) OR number(7) OR number(9);
HEXN(5) <= number(1) OR number(2) OR number(3);
HEXN(6) <= number(0) OR number(1) OR number(7);
	
END Behaviour;