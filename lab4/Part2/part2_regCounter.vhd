LIBRARY ieee; 
USE ieee.std_logic_1164.all ; 
USE ieee.std_logic_unsigned.all; 
ENTITY part2_regCounter IS
	PORT (   SW : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
				KEY : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
				HEX0: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
				HEX1: OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END part2_regCounter; 


ARCHITECTURE Behavior OF part2_regCounter IS

COMPONENT switch_dec
	PORT( S,U,V,W : IN STD_LOGIC;
			num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));

	END COMPONENT switch_dec;
	
COMPONENT eightBitBCD
	PORT(SW: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			tensOut:OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			digitOut:OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END COMPONENT eightBitBCD;
	
COMPONENT char_7seg
		PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT char_7seg;
	
	
	SIGNAL num0 :STD_LOGIC_VECTOR(9 DOWNTO 0);
	SIGNAL num1 :STD_LOGIC_VECTOR(9 DOWNTO 0);
	SIGNAL tens: STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL digits: STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL counter: STD_LOGIC_VECTOR(15 DOWNTO 0);

BEGIN
	process (SW,KEY,counter) 
   begin
      if (KEY(0)'event and KEY(0)='1') then 		
         if SW(0)='1' then   
            counter <= "0000000000000000";
         elsif (SW(1) ='1') then
				counter <= counter + 1;
         end if;
      end if;
   end process;

	A: eightBitBCD PORT MAP (counter(7 DOWNTO 0), tensOut => tens, digitOut => digits);
	N0: switch_dec PORT MAP (digits(3),digits(2),digits(1),digits(0), num => num0);
	N1: switch_dec PORT MAP (tens(3),tens(2),tens(1),tens(0), num => num1);
	H0: char_7seg PORT MAP (num0, HEXN=>HEX0);
	H1: char_7seg PORT MAP (num1, HEXN=>HEX1);
END Behavior ; 


LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY switch_dec IS
	PORT( S,U,V,W : IN STD_LOGIC;
		   num : OUT STD_LOGIC_VECTOR(9 DOWNTO 0));
END switch_dec;

ARCHITECTURE Behaviour of switch_dec IS

BEGIN
	num(0) <= NOT S AND NOT W AND NOT U AND NOT V;
	num(1) <= NOT S AND NOT U AND NOT V AND W;
	num(2) <= NOT S AND NOT U AND V AND NOT W;
	num(3) <= NOT S AND NOT U AND V AND W;
	num(4) <= NOT S AND U AND NOT V AND NOT W;
	num(5) <= NOT S AND U AND NOT V AND W;
	num(6) <= NOT S AND U AND V AND NOT W;
	num(7) <= NOT S AND U AND V AND W;
	num(8) <= S AND NOT U AND NOT V AND NOT W;
	num(9) <= S AND NOT U AND NOT V AND W;
	
END Behaviour;


LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;

ENTITY eightBitBCD IS
	PORT(SW: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			tensOut:OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			digitOut:OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END eightBitBCD;

ARCHITECTURE Behaviour OF eightBitBCD IS
SIGNAL total : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL tens : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL digits : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL result : STD_LOGIC_VECTOR(7 DOWNTO 0);
BEGIN
total <= SW;
PROCESS(total,tens,digits)
BEGIN
	IF (total >= 90) THEN
		tens <= "01011010";
		digits <= "1001";
	ELSIF (total >= 80) THEN
		tens <= "01010000";
		digits <= "1000";
	ELSIF (total >= 70) THEN
		tens <= "01000110";
		digits <= "0111";
	ELSIF (total >= 60) THEN
		tens <= "00111100";
		digits <= "0110";
	ELSIF (total >= 50) THEN
		tens <= "00110010";
		digits <= "0101";
	ELSIF (total >= 40) THEN
		tens <= "00101000";
		digits <= "0100";
	ELSIF (total >= 30) THEN
		tens <= "00011110";
		digits <= "0011";
	ELSIF (total >= 20) THEN
		tens <= "00010100";
		digits <= "0010";
	ELSIF (total >= 10) THEN
		tens <= "00001010";
		digits <= "0001";
	ELSE
		tens <= "00000000";
		digits <= "0000";
	END IF;
END PROCESS;

result <= total - tens;
digitOut <= result(3 DOWNTO 0);
tensOut <= digits;

END Behaviour;





LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY char_7seg IS
	PORT( number: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
	HEXN : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END char_7seg;

ARCHITECTURE Behaviour of char_7seg IS

BEGIN

HEXN(0) <= number(1) OR number(4);
HEXN(1) <= number(5) OR number (6);
HEXN(2) <= number(2);
HEXN(3) <= number(1) OR number(4) OR number(7);
HEXN(4) <= number(1) OR number(3) OR number(4) OR number(5) OR number(7) OR number(9);
HEXN(5) <= number(1) OR number(2) OR number(3);
HEXN(6) <= number(0) OR number(1) OR number(7);
	
END Behaviour;